package ind.rodrigoperalta.challengereba.infrastructure.persona;

import ind.rodrigoperalta.challengereba.common.exception.InvalidDomainException;
import ind.rodrigoperalta.challengereba.common.exception.NotFoundException;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.infrastructure.persona.model.PersonaEntity;
import ind.rodrigoperalta.challengereba.infrastructure.persona.repository.PersonaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PersistenciaPersonaService {

    private final PersonaRepository personaRepository;

    public PersistenciaPersonaService(
        final PersonaRepository personaRepository
    ) {

        this.personaRepository = personaRepository;

    }

    @Transactional
    public void validarPersonaExistente(final Persona persona) {

        final var documento = persona.getDocumento();

        final var exists = this.personaRepository.existsById_TipoDocumentoAndId_NumeroDocumentoAndId_Pais(
            documento.getTipoDocumento().toString(), documento.getNumeroDocumento(), persona.getPais()
        );

        if (exists) {
            throw new InvalidDomainException(
                "Persona con tipo documento {0}, numero {1} y pais {2} ya existe",
                documento.getTipoDocumento().toString(), documento.getNumeroDocumento(), persona.getPais()
            );
        }


    }

    @Transactional
    public boolean existePersonaPorRecursoId(final String id) {

        return this.personaRepository.existsByResourceId(id);

    }

    @Transactional
    public void persistirPersona(final Persona persona) {


        this.personaRepository.findByResourceId(persona.getResourceId())
            .ifPresent(personaEntity -> {

                this.personaRepository.delete(personaEntity);
                this.personaRepository.flush();

            });

        final var entity = PersonaEntity.fromDomain(persona);
        this.personaRepository.save(entity);

    }

    @Transactional
    public Persona obtenerPersonaPorResourceId(final String resourceId) {

        return this.personaRepository
            .findByResourceId(resourceId)
            .orElseThrow(
                () -> new NotFoundException("Persona con resource id {0} no existe", resourceId)
            )
            .toDomain();

    }

}
