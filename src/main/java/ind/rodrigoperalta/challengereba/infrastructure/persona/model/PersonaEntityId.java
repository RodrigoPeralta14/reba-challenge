package ind.rodrigoperalta.challengereba.infrastructure.persona.model;

import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class PersonaEntityId implements Serializable {

    private String tipoDocumento;

    private String numeroDocumento;

    private String pais;

    public static PersonaEntityId fromDomain(
        final Persona persona
    ) {

        final var documento = persona.getDocumento();

        return new PersonaEntityId(
            documento.getTipoDocumento().toString(),
            documento.getNumeroDocumento(),
            persona.getPais()
        );

    }

}
