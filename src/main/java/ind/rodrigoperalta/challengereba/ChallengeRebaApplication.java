package ind.rodrigoperalta.challengereba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengeRebaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChallengeRebaApplication.class, args);
    }

}
