package ind.rodrigoperalta.challengereba.application.persona.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonaCreatedResponseDto {

    // TODO tests unnitarios

    private String id;

    public static PersonaCreatedResponseDto fromDomain(final Persona persona) {

        return new PersonaCreatedResponseDto(
            persona.getResourceId()
        );

    }
}
