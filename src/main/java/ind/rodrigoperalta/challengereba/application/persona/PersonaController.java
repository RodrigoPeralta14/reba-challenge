package ind.rodrigoperalta.challengereba.application.persona;

import ind.rodrigoperalta.challengereba.application.persona.request.PersonaRequestDto;
import ind.rodrigoperalta.challengereba.application.persona.response.PersonaCreatedResponseDto;
import ind.rodrigoperalta.challengereba.application.persona.response.PersonaResponseDto;
import ind.rodrigoperalta.challengereba.domain.persona.actions.CreatePersonaAction;
import ind.rodrigoperalta.challengereba.domain.persona.actions.GetPersonaAction;
import ind.rodrigoperalta.challengereba.domain.persona.actions.UpdatePersonaAction;
import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.TipoDocumento;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("personas")
public class PersonaController {

    private final CreatePersonaAction createPersonaAction;
    private final UpdatePersonaAction updatePersonaAction;
    private final GetPersonaAction getPersonaAction;

    public PersonaController(
        final CreatePersonaAction createPersonaAction,
        final UpdatePersonaAction updatePersonaAction,
        final GetPersonaAction getPersonaAction) {

        this.createPersonaAction = createPersonaAction;

        this.updatePersonaAction = updatePersonaAction;

        this.getPersonaAction = getPersonaAction;
    }

    @PostMapping
    public ResponseEntity<PersonaCreatedResponseDto> crearPersona(
        @RequestBody final PersonaRequestDto personaRequestDto
    ) {

        final var persona = this.createPersonaAction.execute(
            Documento.create(
                TipoDocumento.valueOf(personaRequestDto.getTipoDocumento()),
                personaRequestDto.getNumeroDocumento()
            ),
            personaRequestDto.getPais(),
            LocalDate.parse(personaRequestDto.getFechaNacimiento()),
            Contacto.createContacto(
                personaRequestDto.getEmails(),
                personaRequestDto.getTelefonos()
            )
        );

        return ResponseEntity.ok(
            PersonaCreatedResponseDto.fromDomain(persona)
        );

    }

    @PutMapping("{id}")
    public ResponseEntity<PersonaResponseDto> modificarPersona(
        @PathVariable("id") final String id,
        @RequestBody final PersonaRequestDto personaRequestDto
    ) {

        final var persona = this.updatePersonaAction.execute(
            id,
            Documento.create(
                TipoDocumento.valueOf(personaRequestDto.getTipoDocumento()),
                personaRequestDto.getNumeroDocumento()
            ),
            personaRequestDto.getPais(),
            LocalDate.parse(personaRequestDto.getFechaNacimiento()),
            Contacto.createContacto(
                personaRequestDto.getEmails(),
                personaRequestDto.getTelefonos()
            )
        );

        return ResponseEntity.ok(
            PersonaResponseDto.fromDomain(persona)
        );

    }

    @GetMapping("{id}")
    public ResponseEntity<PersonaResponseDto> obtenerPersona(
        @PathVariable("id") final String id
    ) {

        return ResponseEntity.ok(
            PersonaResponseDto.fromDomain(this.getPersonaAction.execute(id))
        );

    }
}
