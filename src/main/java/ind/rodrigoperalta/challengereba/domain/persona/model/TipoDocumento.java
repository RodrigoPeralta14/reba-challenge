package ind.rodrigoperalta.challengereba.domain.persona.model;

public enum TipoDocumento {

    DNI, PASAPORTE;

}
