package ind.rodrigoperalta.challengereba.domain.persona.model;

import lombok.Value;

import java.util.Objects;

@Value
public class Documento {

    TipoDocumento tipoDocumento;

    String numeroDocumento;

    public static Documento create(
        final TipoDocumento tipoDocumento, final String numeroDocumento) {


        if (Objects.isNull(tipoDocumento)) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Tipo documento is null");
        }

        if (Objects.isNull(numeroDocumento) || numeroDocumento.isBlank()) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Numero documento is null");
        }

        return new Documento(tipoDocumento, numeroDocumento);


    }

}
