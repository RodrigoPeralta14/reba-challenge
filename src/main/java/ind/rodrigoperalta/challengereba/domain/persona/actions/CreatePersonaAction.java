package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CreatePersonaAction {

    private final PersistPersonaAction persistPersonaAction;

    public CreatePersonaAction(final PersistPersonaAction persistPersonaAction) {

        this.persistPersonaAction = persistPersonaAction;

    }

    public Persona execute(
        final Documento documento,
        final String pais,
        final LocalDate fechaNacimiento,
        final Contacto contacto
    ) {

        // crear la persona validada
        final var persona = Persona.create(documento, pais, fechaNacimiento, contacto);

        // persistirla & validar repetidos
        this.persistPersonaAction.execute(persona);

        // retonar persona persistida
        return persona;

    }


}
