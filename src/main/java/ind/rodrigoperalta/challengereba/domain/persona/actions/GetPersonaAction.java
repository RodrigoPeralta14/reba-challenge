package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.infrastructure.persona.PersistenciaPersonaService;
import org.springframework.stereotype.Component;

@Component
public class GetPersonaAction {

    private final PersistenciaPersonaService persistenciaPersonaService;

    public GetPersonaAction(final PersistenciaPersonaService persistenciaPersonaService) {

        this.persistenciaPersonaService = persistenciaPersonaService;

    }

    public Persona execute(final String resourceId) {

        return this.persistenciaPersonaService.obtenerPersonaPorResourceId(resourceId);

    }

}
