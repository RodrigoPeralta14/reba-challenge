package ind.rodrigoperalta.challengereba.domain.persona.model;

import lombok.Value;

import java.util.List;

@Value
public class Contacto {

    List<String> emails;

    List<String> phones;

    public static Contacto createContacto(
        final List<String> emails,
        final List<String> phones) {

        if (emails.isEmpty() && phones.isEmpty()) {
            // TODO change to a custom exception wtih 400
            throw new RuntimeException("Must have at least one non empty list.");
        }

        return new Contacto(emails, phones);

    }

}
