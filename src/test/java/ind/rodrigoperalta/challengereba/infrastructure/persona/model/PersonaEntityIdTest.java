package ind.rodrigoperalta.challengereba.infrastructure.persona.model;

import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class PersonaEntityIdTest {

    @Nested
    @DisplayName("Given a valid domain persona")
    class GivenAValidPersona {

        @Nested
        @DisplayName("When creating PersonaEntityId from domain")
        class WhenCreatingFromDomain {

            @Test
            @DisplayName("Then should return entity")
            void thenShouldReturnEntity() {

                // given
                final var persona = MockUtils.givenValidPersona();

                // when
                final var entity = PersonaEntityId.fromDomain(persona);

                // then
                assertThat(entity)
                    .isNotNull();

                assertThat(entity.getNumeroDocumento())
                    .isNotBlank();

                assertThat(entity.getTipoDocumento())
                    .isNotBlank();

                assertThat(entity.getPais())
                    .isNotBlank();

            }

        }

    }

}
