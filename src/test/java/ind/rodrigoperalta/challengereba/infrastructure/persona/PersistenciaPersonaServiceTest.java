package ind.rodrigoperalta.challengereba.infrastructure.persona;

import ind.rodrigoperalta.challengereba.common.exception.InvalidDomainException;
import ind.rodrigoperalta.challengereba.common.exception.NotFoundException;
import ind.rodrigoperalta.challengereba.infrastructure.persona.model.PersonaEntity;
import ind.rodrigoperalta.challengereba.infrastructure.persona.repository.PersonaRepository;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class PersistenciaPersonaServiceTest {

    private final PersistenciaPersonaService persistenciaPersonaService;

    private final PersonaRepository personaRepository;

    public PersistenciaPersonaServiceTest() {

        this.personaRepository = Mockito.mock(PersonaRepository.class);

        this.persistenciaPersonaService = new PersistenciaPersonaService(
            personaRepository
        );

    }

    @Nested
    @DisplayName("Given a valid persona that is not persisted")
    class GivenAValidPersonaThatIsNotPersisted {

        @Nested
        @DisplayName("When validating if persona is persisted")
        class WhenValidatingIfPersonaIsPersisted {

            @Test
            @DisplayName("Then should not throw exception")
            void thenShouldNotThrowException() {

                // given
                final var persona = MockUtils.givenValidPersona();

                when(personaRepository.existsById_TipoDocumentoAndId_NumeroDocumentoAndId_Pais(any(), any(), any()))
                    .thenReturn(false);

                // when
                persistenciaPersonaService.validarPersonaExistente(persona);

                // then
                verify(personaRepository, times(1))
                    .existsById_TipoDocumentoAndId_NumeroDocumentoAndId_Pais(anyString(), anyString(), anyString());


            }

        }

    }

    @Nested
    @DisplayName("Given a valid persona that is persisted")
    class GivenAValidPersonaThatIsPersisted {

        @Nested
        @DisplayName("When validating if persona is persisted")
        class WhenValidatingIfPersonaIsPersisted {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldNotThrowException() {

                // given
                final var persona = MockUtils.givenValidPersona();
                final var documento = persona.getDocumento();

                when(
                    personaRepository.existsById_TipoDocumentoAndId_NumeroDocumentoAndId_Pais(
                        documento.getTipoDocumento().toString(), documento.getNumeroDocumento(), persona.getPais()
                    )
                )
                    .thenReturn(true);

                // when
                final var thrown = catchThrowable(() -> persistenciaPersonaService.validarPersonaExistente(persona));

                // then
                verify(personaRepository, times(1))
                    .existsById_TipoDocumentoAndId_NumeroDocumentoAndId_Pais(
                        documento.getTipoDocumento().toString(), documento.getNumeroDocumento(), persona.getPais()
                    );

                assertThat(thrown)
                    .isNotNull()
                    .isInstanceOf(InvalidDomainException.class)
                    .hasMessage("Persona con tipo documento %s, numero %s y pais %s ya existe",
                        documento.getTipoDocumento().toString(),
                        documento.getNumeroDocumento(),
                        persona.getPais()
                    );


            }

        }

    }

    @Nested
    @DisplayName("Given valid persona that is not persisted")
    class GivenValidPersona {

        @Nested
        @DisplayName("When persiting nueva persona")
        class WhenPersistingNuevaPersona {

            @Test
            @DisplayName("Then should persist persona")
            void thenShouldPersistPersona() {

                // given
                final var persona = MockUtils.givenValidPersona();

                given(personaRepository.findByResourceId(anyString()))
                    .willReturn(Optional.empty());

                // when
                persistenciaPersonaService.persistirPersona(persona);

                // then
                verify(personaRepository, times(1))
                    .save(any(PersonaEntity.class));

                verify(personaRepository, times(1))
                    .findByResourceId(anyString());


            }

        }

    }

    @Nested
    @DisplayName("Given valid persona that is persisted")
    class GivenValidPersonaThatIsPersisted {

        @Nested
        @DisplayName("When persiting nueva persona")
        class WhenPersistingNuevaPersona {

            @Test
            @DisplayName("Then should persist persona and delete the previously existing one")
            void thenShouldPersistPersonaWhileDeletingThePreviouslyPersistedOne() {

                // given
                final var persona = MockUtils.givenValidPersona();

                given(personaRepository.findByResourceId(anyString()))
                    .willReturn(
                        Optional.of(MockUtils.givenValidPersonaEntityWithResourceId(persona.getResourceId()))
                    );

                // when
                persistenciaPersonaService.persistirPersona(persona);

                // then
                verify(personaRepository, times(1))
                    .save(any(PersonaEntity.class));

                verify(personaRepository, times(1))
                    .delete(any(PersonaEntity.class));

                verify(personaRepository, times(1))
                    .flush();

                verify(personaRepository, times(1))
                    .findByResourceId(anyString());


            }

        }

    }

    @Nested
    @DisplayName("Given non existing resource id")
    class GivenNonExistingResourceId {

        final String resourceId = UUID.randomUUID().toString();

        @Nested
        @DisplayName("When checking if persona exists by resourcce id")
        class WhenCheckingIfPersonaExistsByResourceId {

            @Test
            @DisplayName("Then should return false")
            void thenShouldReturnFalse() {

                // given
                given(personaRepository.existsByResourceId(resourceId))
                    .willReturn(false);

                // when
                final var exists = persistenciaPersonaService.existePersonaPorRecursoId(resourceId);

                // then
                assertFalse(exists);


            }

        }

        @Nested
        @DisplayName("When obtaining persona by resource id")
        class WhenObtainingPersonaByResourceId {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                given(personaRepository.findByResourceId(resourceId))
                    .willReturn(
                        Optional.empty()
                    );

                // when
                final var throwable = catchThrowable(
                    () -> persistenciaPersonaService.obtenerPersonaPorResourceId(resourceId)
                );

                // then
                assertThat(throwable)
                    .isNotNull()
                    .isInstanceOf(NotFoundException.class)
                    .hasMessage("Persona con resource id %s no existe", resourceId);


            }

        }

    }

    @Nested
    @DisplayName("Given existing resource id")
    class GivenExistingResourceId {

        final String resourceId = UUID.randomUUID().toString();

        @Nested
        @DisplayName("When checking if persona exists by resource id")
        class WhenCheckingIfPersonaExistsByResourceId {

            @Test
            @DisplayName("Then should return true")
            void thenShouldReturnFalse() {

                // given
                given(personaRepository.existsByResourceId(resourceId))
                    .willReturn(true);

                // when
                final var exists = persistenciaPersonaService.existePersonaPorRecursoId(resourceId);

                // then
                assertTrue(exists);


            }

        }

        @Nested
        @DisplayName("When obtaining persona by resource id")
        class WhenObtainingPersonaByResourceId {

            @Test
            @DisplayName("Then should return persona")
            void thenShouldReturnPersona() {

                // given
                given(personaRepository.findByResourceId(resourceId))
                    .willReturn(
                        Optional.of(MockUtils.givenValidPersonaEntityWithResourceId(resourceId))
                    );

                // when
                final var personaEntity = persistenciaPersonaService.obtenerPersonaPorResourceId(resourceId);

                // then
                assertThat(personaEntity)
                    .isNotNull();

                assertThat(personaEntity.getResourceId())
                    .isEqualTo(resourceId);


            }

        }

    }


}
