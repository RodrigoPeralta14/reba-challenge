package ind.rodrigoperalta.challengereba.features;

import com.fasterxml.jackson.databind.ObjectMapper;
import ind.rodrigoperalta.challengereba.application.persona.response.PersonaCreatedResponseDto;
import ind.rodrigoperalta.challengereba.application.persona.response.PersonaResponseDto;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class CrudTest {

    final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("[Happy Path] CRUD test")
    void endToEndCrud() throws Exception {

        final var createdPersona = createPersona();
        final var gettedPersona = getPersona(createdPersona.getId());
        final var updatedPersona = updatePersona(gettedPersona);
        // TODO continue with delete

    }


    private PersonaCreatedResponseDto createPersona() throws Exception {

        // given
        final var requestDto = MockUtils.givenValidPersonaRequestDto();

        // when
        final var result = mockMvc.perform(
                post("/personas")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(requestDto))
            )
            .andDo(print());

        final var response = result.andReturn().getResponse();

        // then
        assertThat(result)
            .isNotNull();

        assertThat(response.getStatus())
            .isEqualTo(200);

        assertThat(response.getContentAsString())
            .isNotBlank();

        // TODO check all the response fields individually

        return objectMapper.readValue(response.getContentAsString(), PersonaCreatedResponseDto.class);

    }

    private PersonaResponseDto getPersona(final String id)
        throws Exception {

        final var result = mockMvc.perform(
                get("/personas/" + id)
            )
            .andDo(print());

        final var response = result.andReturn().getResponse();

        // then
        assertThat(result)
            .isNotNull();

        assertThat(response.getStatus())
            .isEqualTo(200);

        assertThat(response.getContentAsString())
            .isNotBlank();

        // TODO check all the response fields individually

        return objectMapper.readValue(response.getContentAsString(), PersonaResponseDto.class);

    }

    private PersonaResponseDto updatePersona(final PersonaResponseDto createdPersona)
        throws Exception {

        // given
        final var requestDto = MockUtils.givenValidPersonaRequestDtoForUpdate();

        // when
        final var result = mockMvc.perform(
                put("/personas/" + createdPersona.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(requestDto))
            )
            .andDo(print());

        final var response = result.andReturn().getResponse();

        // then
        assertThat(result)
            .isNotNull();

        assertThat(response.getStatus())
            .isEqualTo(200);

        assertThat(response.getContentAsString())
            .isNotBlank();

        // TODO check all the response fields individually

        return objectMapper.readValue(response.getContentAsString(), PersonaResponseDto.class);

    }

}
