package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.Persona;
import ind.rodrigoperalta.challengereba.domain.persona.model.TipoDocumento;
import ind.rodrigoperalta.challengereba.infrastructure.persona.PersistenciaPersonaService;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UpdatePersonaActionTest {

    private final PersistenciaPersonaService persistenciaPersonaService;

    private final UpdatePersonaAction updatePersonaAction;

    public UpdatePersonaActionTest() {

        this.persistenciaPersonaService = Mockito.mock(PersistenciaPersonaService.class);

        this.updatePersonaAction = new UpdatePersonaAction(persistenciaPersonaService);

    }

    @Nested
    @DisplayName("Given an existing id, a valid documento, a valid pais, a valid fecha de naacimiento and a valid contacto")
    class GivenAValidExistingIdAndAValidDocumentoAndAValidPaisAndAValidFechaNacimientoAndAValidContacto {

        @Nested
        @DisplayName("When executing action")
        class WhenExecutingAction {

            @Test
            @DisplayName("Then should return updated persona")
            void thenShouldReturnUpdatedPersona() {

                // given
                final var id = UUID.randomUUID().toString();
                final var documento = Documento.create(TipoDocumento.PASAPORTE, "AA6578324AD");
                final var pais = "Chile";
                final var fechaNacimiento = LocalDate.of(1995, 9, 5);
                final var contacto = Contacto.createContacto(
                    Collections.emptyList(),
                    List.of("123456789")
                );

                given(persistenciaPersonaService.obtenerPersonaPorResourceId(id))
                    .willReturn(
                        MockUtils.givenValidPersonaWithId(id)
                    );

                doNothing()
                    .when(persistenciaPersonaService)
                    .persistirPersona(any(Persona.class));

                // when
                final var persona = updatePersonaAction.execute(
                    id, documento, pais, fechaNacimiento, contacto
                );

                // then
                assertThat(persona)
                    .isNotNull();

                assertThat(persona.getResourceId())
                    .isEqualTo(id);

                assertThat(persona.getDocumento().getNumeroDocumento())
                    .isEqualTo("AA6578324AD");

                assertThat(persona.getDocumento().getTipoDocumento())
                    .isEqualTo(TipoDocumento.PASAPORTE);

                assertThat(persona.getPais())
                    .isEqualTo("Chile");

                assertThat(persona.getFechaNacimiento())
                    .isEqualTo(LocalDate.of(1995, 9, 5));

                verify(persistenciaPersonaService, times(1))
                    .obtenerPersonaPorResourceId(id);

                verify(persistenciaPersonaService, times(1))
                    .persistirPersona(any(Persona.class));


            }

        }

    }

    // TODO unhappy paths: null stuff (should throw null pointers), invalid age validation,
    // TODO constraint violation exception for repository if i'm updating to an already used id
}
