package ind.rodrigoperalta.challengereba.domain.persona.model;

import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.catchThrowable;

class PersonaTest {

    @Nested
    @DisplayName("Given a valid documento, valid pais, valid fecha nacimiento, valid datos de contacto")
    class GivenValidDocumentoAndValidPaisAndValidFechaNacimientoAndValidContacto {

        @Nested
        @DisplayName("When creating persona")
        class WhenCreatingPersona {

            @Test
            @DisplayName("Then should return persona")
            void thenShouldReturnPersona() {

                // given
                final var documento = MockUtils.givenValidDocumento();
                final var pais = MockUtils.pais;
                final var fechaNacimiento = LocalDate.parse(MockUtils.fechaNacimiento);
                final var contacto = MockUtils.givenValidContacto();

                // when
                final var persona = Persona.create(documento, pais, fechaNacimiento, contacto);

                // then
                Assertions.assertThat(persona)
                    .isNotNull();

                Assertions.assertThat(persona.getDocumento())
                    .isNotNull();

                Assertions.assertThat(persona.getPais())
                    .isNotNull()
                    .isEqualTo(pais);

                Assertions.assertThat(persona.getFechaNacimiento())
                    .isNotNull()
                    .isEqualTo(fechaNacimiento);

                Assertions.assertThat(persona.getContacto())
                    .isNotNull();

                Assertions.assertThat(persona.getResourceId())
                    .isNotNull();

            }

        }

    }

    @Nested
    @DisplayName("Given a valid documento, valid pais, invalid fecha nacimiento, valid datos de contacto")
    class GivenValidDocumentoAndValidPaisAndInvalidFechaNacimientoAndValidContacto {

        @Nested
        @DisplayName("When creating persona")
        class WhenCreatingPersona {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldReturnPersona() {

                // given
                final var documento = MockUtils.givenValidDocumento();
                final var pais = MockUtils.pais;
                final var fechaNacimiento = LocalDate.now();
                final var contacto = MockUtils.givenValidContacto();

                // when
                final var throwable = catchThrowable(
                    () -> Persona.create(documento, pais, fechaNacimiento, contacto)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Person must be older than 18 years old");


            }

        }

    }

    @Nested
    @DisplayName("Given a null documento, valid pais, valid fecha nacimiento, valid datos de contacto")
    class GivenNullDocumentoAndValidPaisAndValidFechaNacimientoAndValidContacto {

        @Nested
        @DisplayName("When creating persona")
        class WhenCreatingPersona {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldReturnPersona() {

                // given
                final Documento documento = null;
                final var pais = MockUtils.pais;
                final var fechaNacimiento = LocalDate.parse(MockUtils.fechaNacimiento);
                final var contacto = MockUtils.givenValidContacto();

                // when
                final var throwable = catchThrowable(
                    () -> Persona.create(documento, pais, fechaNacimiento, contacto)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Documento must not be null");


            }

        }

    }

    @Nested
    @DisplayName("Given a valid documento, null pais, valid fecha nacimiento, valid datos de contacto")
    class GivenValidDocumentoAndNullPaisAndValidFechaNacimientoAndValidContacto {

        @Nested
        @DisplayName("When creating persona")
        class WhenCreatingPersona {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldReturnPersona() {

                // given
                final var documento = MockUtils.givenValidDocumento();
                final String pais = null;
                final var fechaNacimiento = LocalDate.parse(MockUtils.fechaNacimiento);
                final var contacto = MockUtils.givenValidContacto();

                // when
                final var throwable = catchThrowable(
                    () -> Persona.create(documento, pais, fechaNacimiento, contacto)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Pais must not be null or blank");


            }

        }

    }

    @Nested
    @DisplayName("Given a valid documento, valid pais, null fecha nacimiento, valid datos de contacto")
    class GivenValidDocumentoAndValidPaisAndNullFechaNacimientoAndValidContacto {

        @Nested
        @DisplayName("When creating persona")
        class WhenCreatingPersona {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldReturnPersona() {

                // given
                final var documento = MockUtils.givenValidDocumento();
                final var pais = MockUtils.pais;
                final LocalDate fechaNacimiento = null;
                final var contacto = MockUtils.givenValidContacto();

                // when
                final var throwable = catchThrowable(
                    () -> Persona.create(documento, pais, fechaNacimiento, contacto)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Fecha nacimiento must not be null");


            }

        }

    }

    @Nested
    @DisplayName("Given a valid documento, valid pais, valid fecha nacimiento, null datos de contacto")
    class GivenValidDocumentoAndValidPaisAndValidFechaNacimientoAndNullContacto {

        @Nested
        @DisplayName("When creating persona")
        class WhenCreatingPersona {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldReturnPersona() {

                // given
                final var documento = MockUtils.givenValidDocumento();
                final var pais = MockUtils.pais;
                final var fechaNacimiento = LocalDate.parse(MockUtils.fechaNacimiento);
                final Contacto contacto = null;

                // when
                final var throwable = catchThrowable(
                    () -> Persona.create(documento, pais, fechaNacimiento, contacto)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Contacto must not be null");


            }

        }

    }

}
