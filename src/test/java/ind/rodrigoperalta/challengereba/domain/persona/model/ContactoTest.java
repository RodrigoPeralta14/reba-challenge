package ind.rodrigoperalta.challengereba.domain.persona.model;

import ind.rodrigoperalta.challengereba.domain.persona.model.Contacto;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.catchThrowable;

class ContactoTest {

    @Nested
    @DisplayName("Given valid list of emails and valid list of phones")
    class GivenValidListOfEmailsAndValidListOfPhones {

        @Nested
        @DisplayName("When creating contacto")
        class WhenCreatingContacto {

            @Test
            @DisplayName("Then should return Contacto")
            void thenShouldReturnContacto() {

                // given
                final var emails = List.of(MockUtils.email);
                final var phones = List.of(MockUtils.phone);

                // when
                final var contact = Contacto.createContacto(emails, phones);

                // then
                Assertions.assertThat(contact)
                    .isNotNull();

                Assertions.assertThat(contact.getPhones())
                    .isNotNull()
                    .hasSize(1);

                Assertions.assertThat(contact.getEmails())
                    .isNotNull()
                    .hasSize(1);

            }

        }

    }

    @Nested
    @DisplayName("Given an empty list of emails and an empty list of phones")
    class GivenEmptyListOfEmailsAndEmptyListOfPhones {

        @Nested
        @DisplayName("When creating Contacto")
        class WhenCreatingContacto {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final List<String> emptyEmailList = Collections.emptyList();
                final List<String> emptyPhoneList = Collections.emptyList();

                // when
                final var throwable = catchThrowable(
                    () -> Contacto.createContacto(emptyEmailList, emptyPhoneList)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull();


            }

        }

    }

}
