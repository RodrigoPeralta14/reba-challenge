package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;

import static org.mockito.Mockito.times;

class CreatePersonaActionTest {

    private final CreatePersonaAction createPersonaAction;

    private final PersistPersonaAction persistPersonaAction;

    CreatePersonaActionTest() {

        this.persistPersonaAction = Mockito.mock(PersistPersonaAction.class);

        this.createPersonaAction = new CreatePersonaAction(
            persistPersonaAction
        );

    }


    @Nested
    @DisplayName("Given valid documento, valid pais, valid fecha nacimiento and valid contacto")
    class GivenValidDocumentoAndValidPaisAndValidFechaNacimientoAndValidContacto {

        @Nested
        @DisplayName("When executing action")
        class WhenExecutingAction {

            @Test
            @DisplayName("Then should return persona with parameters inside")
            void thenShouldReturnPersona() {

                // given
                final var documento = MockUtils.givenValidDocumento();
                final var pais = MockUtils.pais;
                final var fechaNacimiento = LocalDate.parse(MockUtils.fechaNacimiento);
                final var contacto = MockUtils.givenValidContacto();

                // when
                final var persona = createPersonaAction.execute(
                    documento,
                    pais,
                    fechaNacimiento,
                    contacto
                );

                // then
                Assertions.assertThat(persona)
                    .isNotNull();

                Mockito.verify(persistPersonaAction, times(1))
                    .execute(persona);


            }

        }

    }

}
