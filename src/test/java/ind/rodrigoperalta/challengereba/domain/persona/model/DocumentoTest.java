package ind.rodrigoperalta.challengereba.domain.persona.model;

import ind.rodrigoperalta.challengereba.domain.persona.model.Documento;
import ind.rodrigoperalta.challengereba.domain.persona.model.TipoDocumento;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.catchThrowable;

class DocumentoTest {

    @Nested
    @DisplayName("Given valid tipo documento and valid numero documento")
    class GivenValidTipoDocumentoAndValidNumeroDocumento {

        @Nested
        @DisplayName("When creating Documento")
        class WhenCreatingDocumento {

            @Test
            @DisplayName("Then should return documento")
            void thenShouldReturnDocumento() {

                // given
                final var tipoDocumento = TipoDocumento.DNI;
                final var numeroDocumento = MockUtils.numeroDocumento;

                // when
                final var documento = Documento.create(tipoDocumento, numeroDocumento);

                // then
                Assertions.assertThat(documento)
                    .isNotNull();

                Assertions.assertThat(documento.getTipoDocumento())
                    .isNotNull()
                    .isEqualTo(TipoDocumento.DNI);

                Assertions.assertThat(documento.getNumeroDocumento())
                    .isNotBlank()
                    .isEqualTo(numeroDocumento);


            }

        }

    }


    @Nested
    @DisplayName("Given null tipo documento and valid numero documento")
    class GivenNullTipoDocumentoAndValidNumeroDocumento {

        @Nested
        @DisplayName("When creating Documento")
        class WhenCreatingDocumento {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final TipoDocumento tipoDocumento = null;
                final var numeroDocumento = MockUtils.numeroDocumento;

                // when
                final var throwable = catchThrowable(
                    () -> Documento.create(tipoDocumento, numeroDocumento)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Tipo documento is null");


            }

        }

    }


    @Nested
    @DisplayName("Given valid tipo documento and null numero documento")
    class GivenValidTipoDocumentoAndNullNumeroDocumento {

        @Nested
        @DisplayName("When creating Documento")
        class WhenCreatingDocumento {

            @Test
            @DisplayName("Then should throw exception")
            void thenShouldThrowException() {

                // given
                final var tipoDocumento = TipoDocumento.DNI;
                final String numeroDocumento = null;

                // when
                final var throwable = catchThrowable(
                    () -> Documento.create(tipoDocumento, numeroDocumento)
                );

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Numero documento is null");


            }

        }

    }

}
