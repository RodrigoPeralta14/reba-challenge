package ind.rodrigoperalta.challengereba.domain.persona.actions;

import ind.rodrigoperalta.challengereba.infrastructure.persona.PersistenciaPersonaService;
import ind.rodrigoperalta.challengereba.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

class PersistPersonaActionTest {

    private final PersistPersonaAction persistPersonaAction;

    private final PersistenciaPersonaService persistenciaPersonaService;

    public PersistPersonaActionTest() {

        this.persistenciaPersonaService = mock(PersistenciaPersonaService.class);

        this.persistPersonaAction = new PersistPersonaAction(
            persistenciaPersonaService
        );

    }

    @Nested
    @DisplayName("Given a person valid to persist")
    class GivenAPersonReadyToPersist {

        @Nested
        @DisplayName("When executing action")
        class WhenExecutingAction {

            @Test
            @DisplayName("Then should call service and persist person successfully")
            void thenShouldCallServiceAndPersistPersonSuccessfully() {

                // given
                final var persona = MockUtils.givenValidPersona();

                doNothing()
                    .when(persistenciaPersonaService)
                    .validarPersonaExistente(persona);

                doNothing()
                    .when(persistenciaPersonaService)
                    .persistirPersona(persona);

                // when
                persistPersonaAction.execute(persona);

                // then
                verify(persistenciaPersonaService, times(1))
                    .validarPersonaExistente(persona);

                verify(persistenciaPersonaService, times(1))
                    .persistirPersona(persona);


            }

        }

    }

    @Nested
    @DisplayName("Given an already persisted person")
    class GivenAnAlreadyPersistedPerson {

        @Nested
        @DisplayName("When executing action")
        class WhenExecutingAction {

            @Test
            @DisplayName("Then should throw exception before persisting")
            void thenShouldThrowExceptionBeforePersisting() {

                // given
                final var persona = MockUtils.givenValidPersona();
                final var exception = new RuntimeException("Persona ya existe");

                doThrow(exception)
                    .when(persistenciaPersonaService)
                    .validarPersonaExistente(persona);

                // when
                final var throwable = catchThrowable(() -> persistPersonaAction.execute(persona));

                // then
                Assertions.assertThat(throwable)
                    .isNotNull()
                    .hasMessage("Persona ya existe");

                verify(persistenciaPersonaService, times(1))
                    .validarPersonaExistente(persona);

                verify(persistenciaPersonaService, times(0))
                    .persistirPersona(any());

            }

        }

    }

}
