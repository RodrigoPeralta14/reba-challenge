FROM maven:3.6.0-jdk-11-slim AS build
COPY src /var/build/app
COPY pom.xml /var/build/app
WORKDIR /var/build/app
RUN mvn clean install

FROM openjdk:11-jre-slim as docker-builded-run
COPY --from=build /var/build/app/target/*.jar /var/run/app/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/var/run/app/app.jar"]

FROM openjdk:11-jre-slim as builded-run
COPY ./target/*.jar /var/run/app/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/var/run/app/app.jar"]
