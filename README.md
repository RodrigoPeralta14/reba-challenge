# Reba Challenge

### By Rodrigo Peralta

---

![Java version](https://img.shields.io/badge/java-11-blue)
![Maven version](https://img.shields.io/badge/maven-3-blueviolet)
![Spring](https://img.shields.io/badge/spring-2.4.9-yellow)
![Lombock](https://img.shields.io/badge/lombock-1.18.20-red)
![junit](https://img.shields.io/badge/junit-4.13-green)
![mockito](https://img.shields.io/badge/mockito-3.3.3-violet)
![h2](https://img.shields.io/badge/h2-1.4.200-limegreen)

---
# Description

- This service allows a full CRUD of the Persona domain class

---

## How to run

- Use local terminal
```bash
mvn clean install
java -jar ./target/challenge-reba-0.0.1-SNAPSHOT.jar
```

- With docker (standalone) (runs clean install inside the container)
```bash
docker-compose up --d --build docker-builded-run
```

- With docker (already built) (assumes you've run the `mvn clean install` command locally and transfers the jar to the docker image)
```bash
docker-compose up --d --build builded-app
```
